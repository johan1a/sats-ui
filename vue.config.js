
module.exports = {
   devServer: {
        proxy: 'https://traeskfindr.com/',
    },
    publicPath: process.env.NODE_ENV === 'production' ? '/sats/' : '/'
}
